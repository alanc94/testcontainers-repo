#Test changes
##The CallRealWebServiceIntegrationTestSpecification test from the project at https://github.com/CognitiveJ/camel-weather did not run successfully and caused a build fail when included  
###I added the Strings "output" to test that the expected values were being used, by parsing the actual values as a string and comparing it to the expected values 
 ```String output = FileUtils.readFileToString(new File(directoryLocation + "/output.xml"), Charset.defaultCharset())
 //test that output returns the expected values for tags
                output.contains("<Time>Jul 05, 2016 - 05:50 AM EDT / 2016.07.05 0950 UTC</Time>")
                println("output.xml contains correct time response")
###when the test passes the cleanup begins by deleting the temp files
 	true
                cleanup: "delete all files"
                deleteFiles(new File(directoryLocation + "/in.csv"), new File(directoryLocation + "/output.xml"))```

##I then created TestExample.groovy to try different ways of testing the app as it was suggested that I use XmlParser and XmlSlurper for testing.
###TestExample.groovy
     
     ```given: "put the CSV into the directory that CAMEL is listening to"
        def parser = new XmlSlurper()
        and: "start WSSimulator"
        WSSimulator.setPort(8866)
        WSSimulator.addSimulation(getClass().getResource("/ws/weather/sample-get-weather-response.yml").getFile() as File);
        WSSimulator.shutdown()
        when:
        waitFor(5, TimeUnit.SECONDS) //let camel do its thing.
        then: "read the written values back into this test so we can output them to a print stream (or going further by validating the response)"
        def output = parser.parse(new File(directoryLocation + '/output.xml'))
        //compare String output with the expected response from sample-get-weather-response
        output.contains("<Time>Jul 05, 2016 - 05:50 AM EDT / 2016.07.05 0950 UTC</Time>")
                println "output:-------\n$output\n-------"
        true
        cleanup: "delete all files"
        deleteFiles(new File(directoryLocation + "/in.csv"), new File(directoryLocation + "/output.xml"))
        WSSimulator.shutdown()```


#Creating a Docker Image
##I created a Dockerfile that uses a jdk image(openjdk:8-jdk-alpine) and adds the camel-weather .jar 

     ```FROM openjdk:8-jdk-alpine
	VOLUME /tmp
	ADD camel-weather-1.0-SNAPSHOT.jar app.jar
	ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]```

##I then made changes to build.gradle, adding the docke plugin and adding the task buildDocker
	
	```task buildDocker(type: Docker, dependsOn: build){
   		applicationName = jar.baseName
   		dockerfile = file('./Dockerfile')         
   		doFirst {
        		copy {
          			from jar
          			into stageDir
                }
        }
}```

###using the command we can build a Docker Image 
     ```./gradlew buildDocker```

###we can then see, tag and push our docker image by using the following commands, my TARGET_IMAGE[:TAG] in this case was my docker cloud username 'alanc25' followed by the name i wanted to give it, 'alanc25/weatherapp'

     ```docker images
  	docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
	docker push TARGET_IMAGE[:TAG]```

#Creating the testcontainer, I created test.groovy which uses docker-compose.yml and pulls "alanc25/weatherapp:latest" from DockerHub, maps the "default.conf", and runs our test on top of the Docker image
##test.groovy creates a generic container for testing alanc25/weather:latest, maps the default.conf and exposes port80
	
     ```static public GenericContainer nginx = new GenericContainer("alanc25/weatherapp:latest")
    		.withClasspathResourceMapping("default.conf", "/etc/nginx/conf.d/default.conf", BindMode.READ_ONLY)
    		.withExposedPorts(80)```

##Under the @Test annotation, we can write groovy tests to test our docker image  

     ```//new test to be written in place of following placeholder
	@Test
	def "Test output return"() {
        	given: "write test for docker image"
        
        	when:
        	waitFor(5, TimeUnit.SECONDS) 
        
		then: "test to see if expected responses are equal to the actual responses"
        	ActualResult = ExpectedResult
    }```

##dockercompose.yml uses the groovy image for running test.groovy
     ```tests:
		image:groovy
		stop_signal: SIGKILL
  		working_dir: $PWD
  		user: root
  	volumes:
    		- $PWD:$PWD
    		- /var/run/docker.sock:/var/run/docker.sock
    		- ~/.groovy/grapes:/root/.groovy/grapes
  	environment:
    		JAVA_OPTS: -DDgroovy.grape.report.downloads=true
  	command: groovy test.groovy```


# testcontainers-repo

##clone repo to directory
```git clone https://alanc94@bitbucket.org/alanc94/testcontainers-repo.git```
