@GrabResolver(name='jitpack', root='https://jitpack.io', m2Compatible='true')
@Grab('com.github.testcontainers:testcontainers-groovy-script:1.4.2')
@groovy.transform.BaseScript(TestcontainersScript)
import org.testcontainers.containers.*
import org.junit.*

@Grab('io.rest-assured:rest-assured:3.0.3')
@GrabExclude("org.codehaus.groovy:groovy-xml")
import io.restassured.*
import static io.restassured.RestAssured.*
import static org.hamcrest.Matchers.*

@groovy.transform.Field
@ClassRule
static public GenericContainer nginx = new GenericContainer("alanc25/weatherapp:latest")
    .withClasspathResourceMapping("default.conf", "/etc/nginx/conf.d/default.conf", BindMode.READ_ONLY)
    .withExposedPorts(80)

@BeforeClass
static void setup() {
    RestAssured.baseURI = "http://${nginx.containerIpAddress}:${nginx.firstMappedPort}"
}

//new test to be written in place of following
@Test
void "check running message"() {
    1==1
}

